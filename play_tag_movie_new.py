import time
from pylsl import StreamInfo, StreamOutlet
import subprocess

from dummy_stream import start_dummy_stream


def start_stream(filename):
    info = StreamInfo('Ganglion_EEG', 'Markers', 1, 0.0, 'int32', 'marker')
    outlet = StreamOutlet(info)

    outlet.push_sample([-1], time.time())

    outlet.push_sample([1], time.time())
    #start_dummy_stream()
    subprocess.call(['mpv', filename])
    outlet.push_sample([2], time.time())
