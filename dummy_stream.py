import time
import pandas as pd
from pylsl import StreamInfo, StreamOutlet


def start_dummy_stream():
    info = StreamInfo(name='Muse_EEG', type='EGG', channel_count=1)
    outlet = StreamOutlet(info)

    outlet.push_sample([-1], time.time())

    df = pd.read_csv('data_bunny_pierre_1.csv')

    for idx, val in enumerate(df['timestamps'].tolist()):
        time.sleep(0.1)
        outlet.push_sample([val])
        if idx > 30:
            break
