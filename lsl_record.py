#!/usr/bin/env python
# code by Alexandre Barachant
import numpy as np
import pandas as pd
from time import time
from pylsl import StreamInlet, resolve_byprop, LostError


def get_channel_names(inlet):
    info = inlet.info()
    description = info.desc()

    Nchan = info.channel_count()

    ch = description.child('channels').first_child()
    ch_names = [ch.child_value('label')]
    for i in range(1, Nchan):
        ch = ch.next_sibling()
        ch_names.append(ch.child_value('label'))

    return ch_names


def output_recording(res, timestamps, markers, ch_names, output_filename):
    res = np.concatenate(res, axis=0)
    timestamps = np.array(timestamps)

    res = np.c_[timestamps, res]
    data = pd.DataFrame(data=res, columns=['timestamps'] + ch_names)

    data['Marker'] = 0
    # process markers:
    for marker in markers:
        # find index of markers
        ix = np.argmin(np.abs(marker[1] - timestamps))
        data.loc[ix, 'Marker'] = marker[0][0]

    data.to_csv(output_filename, float_format='%.3f', index=False)

    print('Done !')


def start_recording(output_filename):
    print("looking for a Markers stream...")

    marker_streams = resolve_byprop('type', 'Markers', timeout=2)
    if len(marker_streams) == 0:
        raise RuntimeError("Cant find Markers stream")

    print("Found markers stream")
    inlet_marker = StreamInlet(marker_streams[0])

    print("looking for an EEG stream...")
    streams = resolve_byprop('type', 'EGG', timeout=2)

    if len(streams) == 0:
        raise RuntimeError("Cant find EEG stream")

    print("Start acquiring data")
    inlet = StreamInlet(streams[0], max_chunklen=12)


    ch_names = get_channel_names(inlet)

    res = []
    timestamps = []
    markers = []
    t_init = time()
    print('Start recording at time t=%.3f' % t_init)
    while True:
        try:
            data, timestamp = inlet.pull_chunk(timeout=1.0,
                                               max_samples=12)
            if timestamp:
                res.append(data)
                timestamps.extend(timestamp)
            if inlet_marker:
                marker, timestamp = inlet_marker.pull_sample(timeout=0.0)
                if timestamp:
                    markers.append([marker, timestamp])
        except LostError:
            break

    output_recording(res, timestamps, markers, ch_names, output_filename)